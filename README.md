# Install

1. Make sure Emacs 25.3+ is installed (Doom Emacs suggests 26.1)

2. $
    
    git clone https://github.com/hlissner/doom-emacs .emacs.d
    
    cd .emacs.d
    
    git checkout develop
    
    bin/doom quickstart


3. Remove produced .doom.d and replace with this repository
