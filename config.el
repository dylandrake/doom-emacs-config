;;; ~/.doom.d/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here

; Re enable 's' in normal mode
(after! evil-snipe
  (evil-snipe-mode -1))

; Line numbers
(setq display-line-numbers-type nil)

; Indents -> spaces
(setq indent-tabs-mode nil)


; Language specific settings
; JavaScript
(setq js-indent-level 2)

; Css
(setq css-indent-level 2)
